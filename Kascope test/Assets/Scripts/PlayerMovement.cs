﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    #region Variables

    public CharacterController controller;
    public AudioClip runSound;
	public AudioSource audioSource;
    public bool soundIsPlaying;

    public float runSpeed = 12f;
    public float sprintSpeed = 20f;
    public float moveSpeed = 12f;

    public float gravity = -9.81f;
    public float jumpHeight = 3f;

    public Transform groundCheck;
    public float groundDistance = 0.1f;   
    public LayerMask groundMask;

    public GameManager gameManager;

    Vector3 velocity;
    bool isGrounded;

    #endregion

    #region Main Methods

    void Start(){
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        audioSource.GetComponent<AudioSource>().clip = runSound;
    }

    void Update()
    {
        Move();
    }

    #endregion

    #region Movement Methods

    // Handles Player jumping
    void Jump(){
        velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
    }

    // Handles Player movement
    void Move(){
        
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if(isGrounded && velocity.y < 0){
            velocity.y = -2f;
            
        }

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;

        controller.Move(move * moveSpeed * Time.deltaTime);

        if(Input.GetButton("Sprint")){
            Sprint();
        }

        if(Input.GetButtonDown("Jump") && isGrounded){
            Jump();
        }

        if((move.x > .1f || move.z > .01f) && isGrounded){
            if(!audioSource.isPlaying){
                audioSource.Play();
            }
        }else if(gameManager.gameIsPaused){
            audioSource.Stop();
        }
        else{
            audioSource.Stop();
        }

        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);
    }

    // Handles Player sprinting
    void Sprint(){
        if(gameManager.sprintValue > 0f){
            moveSpeed = sprintSpeed;
            gameManager.sprintValue -= 1f * Time.deltaTime;
            if(gameManager.sprintValue < 0f){
                gameManager.sprintValue = 0f;
            }
        }
        else{
            moveSpeed = runSpeed;
        }
    }

    #endregion

    #region Helper Methods

    // Checks for Trigger Collisions
    void OnTriggerEnter(Collider other){
        Debug.Log("Collision with: " + other.gameObject.name);

        // End Zone Trigger
        if(other.gameObject.tag == "EndTrigger"){
            audioSource.enabled = false;
            Debug.Log("End of Level Triggered");
            gameManager.EndScreen();
        }

        // Full Auto PowerUp Trigger
        if(other.gameObject.tag == "PowerUp.FullAuto"){
            Debug.Log("Full Auto enabled");
            StartCoroutine(gameManager.CountDown(other, gameManager.fullAutoTimer));

        }

        // Sprint PowerUP
        if(other.gameObject.tag == "PowerUp.Sprint"){
            Debug.Log("Sprint refill");
            StartCoroutine(gameManager.ChangeValueOerTime(gameManager.sprintValue, gameManager.maxSprintValue, 3f));
        }
    }

    #endregion

}
