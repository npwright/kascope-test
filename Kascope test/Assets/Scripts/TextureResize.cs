﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class TextureResize : MonoBehaviour 
{

    #region Variables

    public float scaleFactor = 5.0f;
    Material mat;

    #endregion

    #region Main Methods

    void Start () 
    {
        GetComponent<Renderer>().sharedMaterial.mainTextureScale = new Vector2 (transform.localScale.x / scaleFactor , transform.localScale.z / scaleFactor);
    }

    // Update is called once per frame
    void Update () 
    {

        if (transform.hasChanged && Application.isEditor && !Application.isPlaying) 
        {
            GetComponent<Renderer>().sharedMaterial.mainTextureScale = new Vector2 (transform.localScale.x / scaleFactor , transform.localScale.z / scaleFactor);
            transform.hasChanged = false;
        } 

    }

    #endregion
}