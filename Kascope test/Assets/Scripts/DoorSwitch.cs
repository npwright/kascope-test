﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorSwitch : MonoBehaviour
{

    #region Variables

    public GameObject door;
    public bool doorIsOpening;
    public float endPos;
    public float speed;

    #endregion

    #region Main Methods

    void Update(){
        if(doorIsOpening){
            door.transform.Translate(Vector3.up * Time.deltaTime * speed);

        }
        if(door.transform.position.y > endPos){
            doorIsOpening = false;

        }
    }

    #endregion

    #region Helper Methods

    // toggles when linked Door is opening or opened
    public void OpenDoor(){
        doorIsOpening = true;
    }

    #endregion
}
