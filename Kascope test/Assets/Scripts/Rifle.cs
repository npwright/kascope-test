﻿using UnityEngine;

public class Rifle : MonoBehaviour
{

    #region Variables

    public float damage = 10;
    public float range = 100;
    public float interactRange = 5f;
    public float fireRate = 5f;

    public Camera fpscam;
    public ParticleSystem muzzleFlash;
    public ParticleSystem cartridgeEffect;
    public GameObject impactEffect;
    private float nextTimeToFire = 0f;

    public AudioClip shoot;
	public AudioSource audioSource;

    public GameManager gameManager;

    #endregion

    #region Main Methods

    void Start(){
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    void Update()
    {
        if (Input.GetButtonDown("Fire1") && Time.time >= nextTimeToFire && Time.timeScale > 0 && !gameManager.fullAuto){

            nextTimeToFire = Time.time + 1f/fireRate;
            Shoot();

        }else if (Input.GetButton("Fire1") && Time.time >= nextTimeToFire && Time.timeScale > 0 && gameManager.fullAuto){
            nextTimeToFire = Time.time + 1f/(fireRate * 3);
            Shoot(); 
        }

        if(Input.GetButtonDown("Submit")){
            Debug.Log("Submit pressed");
            Interact();
        }
    }

    #endregion

    #region Helper Methods

    // Handles Player shooting
    void Shoot(){
        RaycastHit hit;

        audioSource.GetComponent<AudioSource>().clip = shoot;
		audioSource.Play();
        muzzleFlash.Play();
        cartridgeEffect.Play();
        if(Physics.Raycast(fpscam.transform.position, fpscam.transform.forward, out hit, range)){
            Target target = hit.transform.GetComponent<Target>();
            if(target != null){
                target.TakeDamage(damage);
            }

            GameObject impactGO = Instantiate(impactEffect, hit.point, Quaternion.LookRotation(hit.normal));
            Destroy(impactGO, 2f);
            
        }
    }

    // Handles Player interaction with objects and buttons
    void Interact(){
        Debug.Log("Calling Interact");
        RaycastHit hit;

        if(Physics.Raycast(fpscam.transform.position, fpscam.transform.forward, out hit, interactRange) && hit.transform.gameObject.tag == "Switch"){
            Debug.Log("Interacting with: " + hit.transform.gameObject.name);
            DoorSwitch doorSwitch = hit.transform.GetComponent<DoorSwitch>();
            if(doorSwitch != null){
                doorSwitch.OpenDoor();
            }

        }
    }

    #endregion
}
