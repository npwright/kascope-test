﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{

    #region Variables

    public GameManager gameManager;
    public AudioClip downSound;
    bool isDown = false;
    public float pauseDuration;

	public AudioSource audioSource;

    [System.Serializable]
    public enum TargetType{
        ENEMY,
        NOSHOOT,
        DEFAULT
    }

    public TargetType targetType;

    public float health = 50f;

    #endregion

    #region Main Methods

    void Start(){
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    #endregion

    #region Helper Methods

    // subtracts damage from health
    public void TakeDamage(float amount){
        if(!isDown){
            health -= amount;
            Debug.Log( this.name +" health: "+ health);
            if(health <= 0){
                gameManager.targetsHit += 1;
                Die();
            }
        }
    }

    // Handles target death
    void Die(){
        gameObject.GetComponent<Animation> ().Play("target_down");
		audioSource.GetComponent<AudioSource>().clip = downSound;
		audioSource.Play();
        isDown = true;
        StartCoroutine(gameManager.TimerPause(pauseDuration));
    }

    #endregion
}
