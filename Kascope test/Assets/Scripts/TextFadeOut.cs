﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TextFadeOut : MonoBehaviour
{

    #region Variables

    public GameManager gameManager;
    public float fadeOutTime;
    public TextMeshProUGUI text;
    
    #endregion

    #region Main Methods

    void Start(){
        FadeOut();
    }

    #endregion

    #region Helper Methods

    // Starts text FadeOut
    public void FadeOut(){
        StartCoroutine(FadeOutRoutine());
    }

    //IE Numerator that handles text fade Lerping
    IEnumerator FadeOutRoutine(){
        Color originalColor = text.color;
        for(float t = 0.01f; t < fadeOutTime; t += Time.deltaTime){
            text.color = Color.Lerp(originalColor, Color.clear, Mathf.Min(1, t/fadeOutTime));
            yield return null;
        }
    }

    #endregion
}
